# -*- coding: utf-8 -*-
"""
Created on Sun Oct 22 21:37:02 2017

@author: fl0w-1
"""

numbers = '4.52;1.23;8.65;1.4'

#String wird gesplitted, listen für gerundet und dictionary initialisiert
numbers = numbers.split(';')
ints = list()
mydic = dict()

for (i, num) in enumerate(numbers):
    #iteriert über jedes Listenelement und:
    numbers[i] = float(numbers[i])
    #castet strings zu float
    ints.append(int(round(numbers[i])))
    #rundet Element in eine neue Liste ints
    mydic[ints[i]] = numbers[i]
    #keys werden werte zugeteilt
    #letzer key überschreibt gleichen vorherigen

for key in mydic:
    print 'Schluessel = ', key, '; Wert = ', mydic[key]