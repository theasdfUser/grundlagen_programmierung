# -*- coding: utf-8 -*-


import random

def calcVolume(params):
    
    side    = float(params[0].split('=')[1])
    height  = float(params[1].split('=')[1])
    
    volume  = (side**2)*height*(1./3)
    
    Pyramide = {'Hoehe' : height, 'Seitenlaenge' : side, 'Volumen' : volume}
    
    return(Pyramide)


print calcVolume(['Seitenlaenge = 7.2', 'Hoehe = 4.52'])

print '\n----Tests:----'
for i in range(0, 10):
   print calcVolume(['Seitenlaenge = %f' % random.uniform(0, 10), 'Hoehe = %f' % random.uniform(0, 10)])