# -*- coding: utf-8 -*-
"""
Hausaufgabe 1
Grundlagen Programmieren
Seeling Florian
Mat 11707260
"""

#Aufgabe 1.1

def print_gfunction(a,b,c,x):
    #Variablen nicht private?  
    print 'x = ', x, ', g(x)= ', (a + (x/b) + x**c)

print_gfunction(14.2, 4, 2, 2)
# Ergebnis um 0.5 verfälscht, wenn b,x int sind, da 2/4=0 gerundet wird

print_gfunction(14.2, 4, 2, 1)
print_gfunction(14.2, 4, 2, 1.)
# Siehe oben. Wenn zumindest enweder b oder x als float übergeben werden
# stimmt das Ergebnis.



