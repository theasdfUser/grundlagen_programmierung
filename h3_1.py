# -*- coding: utf-8 -*-


def simplecount(text, buchstaben):
    output = dict()
    for key in str(buchstaben): #casten um TypeError zu vermeiden
        output[key] = str(text).count(key)
    return output


def charcount(text, buchstaben):
    
    output = dict()
    
    #dictionary muss initalisiert werden (null+1 nicht möglich)
    for key in str(buchstaben):
        output[key] = 0
    
    for item in str(text):
        #über output statt list(buchstaben) iterieren, damit Duplikate in buchstaben nicht berücksichtigt werden
        for character in output:
            if item == character:
                output[character] = output[character] + 1
    
    return output


#Überprüfung
print charcount('Teststring', 'teststring')
print charcount('W4rum s011 d13 v4r14613 buchstaben h31553n? 4113 Z31ch3n funkt10n13r3n.', 123456789)

print 'Simple Lösung: (habe mich zu spät erinnert, dass str.count() existiert)'
print simplecount('Teststring', 'teststring')
print simplecount('W4rum s011 d13 v4r14613 buchstaben h31553n? 4113 Z31ch3n funkt10n13r3n.', 123456789)