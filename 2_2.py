# -*- coding: utf-8 -*-

import random

def calculateMinMax(a,b,c):

    #Ableitung: 2*a*x + b = 0
    ext = [0, 0]
    ext[0] = -b / (2*a)
    ext[1] = a*ext[0]**2 * b*ext[0] + c
    
    if      a == 0:
        return 'Lineare Funktion, keine Min/Max'
    
    elif    a >= 1:
        return 'Minimum:', ext
    
    else:
        return 'Maximum:', ext

for i in range(0, 10):
    print calculateMinMax(random.uniform(-10, 10), random.uniform(-10, 10), random.uniform(-10, 10))