# -*- coding: utf-8 -*-

import random

def calculate(a, b, c):
    
    if b**2 >= 4*a*c:
        x1 = (-b + (b**2 - 4*a*c)**(1./2)) / 2*a
        x2 = (-b - (b**2 - 4*a*c)**(1./2)) / 2*a
        return (x1, x2)
    else:
        return 'Keine reellen Lösungen'

for i in range(0, 20):
    print calculate(random.uniform(0, 10), random.uniform(0, 10), random.uniform(0, 10))