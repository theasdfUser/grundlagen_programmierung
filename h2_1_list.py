# -*- coding: utf-8 -*-


zahlen = [2, 2.5, 3, 'Vier', 9]

print '2.5 in Liste?', 2.5 in zahlen

print 'Stelle von 2.5 in Liste:', zahlen.index(2.5)+1

beliebigeVariable = zahlen.pop(-2)
print 'vorletztes Element wurde aus Liste ausgeschnitten'

print 'Listenlänge nach ausschneidem von vorletzem Element:', len(zahlen)

zahlen.append(beliebigeVariable)
print 'Vorher ausgeschnittenes Element wurde wieder angehängt'

print 'Liste verdreifacht:', zahlen*3

werte = [0]
print 'Neue Liste', werte, ' erstellt und mit alter addiert:', zahlen+werte

doppelListe = [zahlen, werte]
print 'Liste mit Listen erstellt'

newList = list(doppelListe)
print 'ListList explizit kopiert'

del newList[0]
print 'alte Liste bleibt gleich nach löschen eines Teils der neuen:', doppelListe
newList.append(1)
print 'alte Liste bleibt nach annhängen an neue gleich:', doppelListe