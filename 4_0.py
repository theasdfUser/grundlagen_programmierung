# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 01:51:47 2017

@author: fl0w-1
"""

lagerAutos = dict(Audi=9, BMW=7, VW=4)
auto = 'Audi'

def autoLagernd(lagerAutos, auto):
    if auto in lagerAutos:
        return '%s %s lagernd' %(lagerAutos[auto], auto)
    
    else:
        return 'Kein %s lagernd' %(auto)
    
print autoLagernd(lagerAutos, auto)


for key in lagerAutos:
    print key, ':', lagerAutos[key]
    

def maxLagernd(lagerAutos):
    
    maxAutos = int()
    
    for key in lagerAutos:
        if lagerAutos[key] >= maxAutos:
            maxAutos = lagerAutos[key]
            maxName = []
            maxName.append(key)
    
    return (maxAutos, maxName)

print maxLagernd(lagerAutos)