# -*- coding: utf-8 -*-
"""
Hausaufgabe 1
Grundlagen Programmieren
Seeling Florian
Mat 11707260
"""

#Aufgabe 1.2

import random

Anzahl_Bytes = random.randint(1, 10)
bits = Anzahl_Bytes*8
print 'Bytes: ', Anzahl_Bytes, '\nunique numbers: ', 2**bits, '\nhighest number (no negatives): ', 2**bits-1, '\nlowest number (actual int): ', -2**(bits-1), '\nhighest number (actual int): ', 2**(bits-1)-1
